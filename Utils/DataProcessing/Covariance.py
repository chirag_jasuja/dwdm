import plotly.graph_objects as go
import pandas as pd
from pymongo import MongoClient
import os

client = MongoClient('192.168.29.203', 27017)


def get_data(collection, query):
    """ start and end date should be in a format "yyyy-mm-dd" """
    if collection not in ["AirPollution", "Cases", "PopulationStats", "Weather"]:
        print(f"collection name {collection} not in database")
        raise
    db = client["DWDM"]
    col = db[collection]
    results = col.find(query)
    return results


def population_covid_co_var(CPP=None):
    cases_df = pd.DataFrame(get_data("Cases", {"Date": {'$regex': '.*'}}))
    cases_df = cases_df[["District", "Confirmed", "Recovered", "Deceased"]]
    # Index(['_id', 'Date', 'State', 'District', 'Confirmed', 'Recovered', 'Deceased', 'Other', 'Tested']
    cases_df = cases_df.groupby("District").sum()
    cases_df["District"] = cases_df.index.to_frame()
    cases_df["District"] = cases_df["District"].str.lower()
    cases_df.index.name = None

    population_df = pd.DataFrame(get_data("PopulationStats", {"District name": {"$regex": '.*'}}))
    # Index(['_id', 'State name', 'District name', 'Population', 'Male', 'Female','Literate', 'Male_Literate'
    population_df.drop(columns=["State name", "_id"], inplace=True)
    population_df.rename(columns={'District name': "District"}, inplace=True)
    population_df["District"] = population_df["District"].str.lower()
    df = pd.merge(cases_df, population_df, on="District", how="outer")
    df.dropna(inplace=True)
    df["Confirmed per person"] = df[["Confirmed"]].div(df["Population"], axis=0)
    df["Deceased per person"] = df[["Deceased"]].div(df["Population"], axis=0)
    df["Deceased per confirmed"] = df[["Deceased"]].div(df["Confirmed"], axis=0)
    pd.options.display.max_rows = 1000
    columns = list(df.keys())
    columns = [ele for ele in columns if
               ele not in ['Confirmed', 'Recovered', 'Deceased', 'Confirmed per person', 'Deceased per person',
                           'Deceased per confirmed', 'District']]
    df[columns] = df[columns].div(df["Population"], axis=0)

    corr = df.corr()
    corr["Category"] = corr.index.to_frame()
    corr = corr[
        ~corr["Category"].isin(
            ["Population", 'Confirmed', 'Recovered', 'Deceased', 'Confirmed per person', 'Deceased per person',
             'Deceased per confirmed', 'District'])]
    print(corr["Deceased per confirmed"].sort_values())
    print("")
    print(corr["Deceased per person"].sort_values())
    print("")
    # corr.drop(columns=["Confirmed per person"], inplace=True)
    print(corr["Confirmed per person"].sort_values())


def crosscorr(datax, datay, lag=0, wrap=False):
    """ Lag-N cross correlation.
    Shifted data filled with NaNs

    Parameters
    ----------
    lag : int, default 0
    datax, datay : pandas.Series objects of equal length
    Returns
    ----------
    crosscorr : float
    """
    if wrap:
        shiftedy = datay.shift(lag)
        shiftedy.iloc[:lag] = datay.iloc[-lag:].values
        return datax.corr(shiftedy)
    else:
        return datax.corr(datay.shift(lag))


def air_pollution_covid_co_var(city_list):
    c = None
    for city in city_list:
        print(city)
        # try:
        cases_df = pd.DataFrame(get_data("Cases", {"District": city}))
        cases_df.rename(columns={"District": "city", "Date": "date"}, inplace=True)
        pollution_df = pd.DataFrame(get_data("AirPollution", {"city": city}))
        df = pd.merge(cases_df, pollution_df, on="date", how="outer")
        df = df[['city_x', 'Confirmed', 'Recovered', 'Deceased', 'co', 'no2', 'o3', 'pm10', 'pm25', 'so2', 'date']]
        df["Deceased per recovered"] = df["Deceased"].div(df["Recovered"], axis=0)
        # print(df.corr()["Deceased"][3:])
        d1 = df['Deceased per recovered']
        d2 = df['co']
        rs = [round(crosscorr(d1, d2, lag), 2) for lag in range(-int(15), int(15))]
        print(rs)
        # break

        #
        # if c is None:
        #     c = df.corr()["Deceased"][3:]
        # else:
        #     c = pd.concat([c, df.corr()["Deceased"][3:]], axis=1)
        # break
        # except Exception as e:
        #     print(e)
        #     print(city)
    # population
    print(c)
    pass


def weather_covid_co_var(city_list):
    c = None
    for city in city_list:
        print(city)
        cases_df = pd.DataFrame(get_data("Cases", {"District": city}))
        cases_df.rename(columns={"District": "city", "Date": "date"}, inplace=True)
        pollution_df = pd.DataFrame(get_data("Weather", {"city": city}))
        df = pd.merge(cases_df, pollution_df, on="date", how="outer")
        df = df[['date', 'Confirmed', 'Recovered', 'Deceased', 'Other', 'Tested', 'maxtempC', 'mintempC', 'avgtempC',
                 'sunHour', 'uvIndex']]
        df["Deceased per recovered"] = df["Deceased"].div(df["Recovered"], axis=0)
        print(df.corr()["Deceased"][3:])
        # d1 = df['Deceased per recovered']
        # d2 = df['co']
        # rs = [round(crosscorr(d1, d2, lag),2) for lag in range(-int(15), int(15))]
        # print(rs)
        print(df.keys())
        # break

        #
        if c is None:
            c = df.corr()["Deceased per recovered"][3:]
        else:
            c = pd.concat([c, df.corr()["Deceased per recovered"][3:]], axis=1)
        # break
        # except Exception as e:
        #     print(e)
        #     print(city)
    # population
    pd.options.display.max_columns = 1000

    print(c)
