from datetime import datetime
import dash_bootstrap_components as dbc

import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import dash_table
from .graphs import corona_cases_graph, get_data, air_pollution_graph, weather_graph

Dcc = pd.DataFrame.from_dict({"Parameter": ["Not_having_bathing_facility_within_the_premises_Total_Households",
                                            "Location_of_drinking_water_source_Away_Households", "Age_Group_0_29",
                                            "Higher_Education", "Having_bathing_facility_Total_Households", "Sikhs"],
                              "Value": [-0.414, -0.293, -0.245, +0.344, +0.376, +0.451]})

Dcp = pd.DataFrame.from_dict(
    {"Parameter": ["Below_Primary_Education", "Not_having_bathing_facility_within_the_premises_Total_Households",
                   "Illiterate_Education", "Urban_Households", "Households_with_Computer", "Households_with_Internet"],
     "Value": [-0.295, -0.272, -0.269, +0.442, +0.453, +0.516]})

Ccp = pd.DataFrame.from_dict({"Parameter": ["Household_size_6_8_persons_Households", "Below_Primary_Education",
                                            "Age_Group_0_29", "Urban_Households", "Households_with_Computer",
                                            "Households_with_Internet"],
                              "Value": [-0.470, -0.448, -0.440, +0.565, +0.620, +0.654]})


def start(city_list):
    app = dash.Dash(title="DWDM Project", external_stylesheets=[dbc.themes.BOOTSTRAP])

    df = pd.DataFrame(get_data("Cases", {"Date": {'$regex': '^2020-09.*'}}))
    India_States = sorted(list(df["State"].unique()))

    country_corona_controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dbc.Label("Type of Graph"),
                    dcc.Dropdown(
                        id="day_total_country",
                        placeholder="Select type of Graph",
                        options=[
                            {'label': "Daywise", "value": "Daywise"},
                            {'label': "Cumulative", "value": "Cumulative"}
                        ],
                        value="Cumulative"
                    ),
                ]
            )
        ],
        body=True,
    )
    state_corona_controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dbc.Label("State"),
                    dcc.Dropdown(
                        id='states_dropdown_state_corona',
                        placeholder="Select a State",
                        options=[{'label': k, 'value': k} for k in India_States],
                        value=["Maharashtra"],
                        multi=True,
                        searchable=False
                    ),
                    dbc.Label("Type of Graph"),
                    dcc.Dropdown(
                        id="day_total_state",
                        placeholder="Select type of Graph",
                        options=[
                            {'label': "Daywise", "value": "Daywise"},
                            {'label': "Cumulative", "value": "Cumulative"}
                        ],
                        value="Cumulative"
                    ),
                ]
            )
        ],
        body=True,
    )
    district_corona_controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dbc.Label("State"),
                    dcc.Dropdown(
                        id='states_dropdown_district_corona',
                        placeholder="Select a State",
                        options=[{'label': k, 'value': k} for k in India_States],
                        value=["Delhi"],
                        multi=True,
                        searchable=False
                    ),
                    dbc.Label("District"),
                    dcc.Dropdown(
                        id='district_dropdown_corona', multi=True, searchable=False,
                        placeholder="Select a District",
                        value=["Delhi"]
                    ),
                    dbc.Label("Type of Graph"),
                    dcc.Dropdown(
                        id="day_total_district",
                        placeholder="Select type of Graph",
                        options=[
                            {'label': "Daywise", "value": "Daywise"},
                            {'label': "Cumulative", "value": "Cumulative"}
                        ],
                        value="Cumulative"
                    ),

                ]
            )
        ],
        body=True,
    )
    city_weather_controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dbc.Label("City"),
                    dcc.Dropdown(
                        id='states_dropdown_district_weather',
                        placeholder="Select a City",
                        options=[{'label': k, 'value': k} for k in city_list],
                        value=["Delhi"],
                        multi=True,
                        searchable=False
                    ),

                ]
            )
        ],
        body=True,
    )
    city_air_pollution_controls = dbc.Card(
        [
            dbc.FormGroup(
                [
                    dbc.Label("City"),
                    dcc.Dropdown(
                        id='states_dropdown_district_pollution',
                        placeholder="Select a City",
                        options=[{'label': k, 'value': k} for k in city_list],
                        value=["Delhi"],
                        multi=True,
                        searchable=False
                    ),
                ]
            )
        ],
        body=True,
    )
    card_style = {
        "box-shadow": "0 4px 8px 0 rgba(0,0,0,0.2)",
        "transition": "0.3s",
        "background-color": "#FFFFFF",
        "padding": "75px auto 75px",
        "margin": "10px auto 25px",
        "border-radius": "15px"
    }
    html_list = [
        dbc.Container([
            html.H2("Covid Data WareHouse", style={"text-align": "center", "padding-top": "10px"}),
            html.H6("Data Warehousing & Data Mining Project", style={"text-align": "center", "padding": "10px"}),
            # html.P(["Chirag Jasuja 2k17/CO/096", html.Br(), "Harshit Gupta 2k17/CO/132"], style={"text-align": "right"})
        ], id="Header", fluid=True, style=card_style),
        dbc.Container([
            html.H3("Corona Graphs", style={"padding-top": "10px"}),
            html.Hr(),
            dbc.Container([
                html.H5("Country Level"),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(country_corona_controls, md=3),
                        dbc.Col(dcc.Graph(
                            id=f'country_corona_graph',
                            figure=corona_cases_graph("Country", ["India"],
                                                      ["Recovered", "Confirmed", "Deceased", "Active"],
                                                      "Cumulative")
                        ), md=9),
                    ],
                    align="center",
                ),

            ], id="country-corona", fluid=True),

            dbc.Container([
                html.H5("State Level"),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(state_corona_controls, md=3),
                        dbc.Col(dcc.Graph(
                            id=f'state_corona_graph',
                            figure=corona_cases_graph("State", ["Maharashtra"],
                                                      ["Recovered", "Confirmed", "Deceased", "Active"],
                                                      "Cumulative")
                        ), md=9),
                    ],
                    align="center",
                ),
            ], id="state-corona", fluid=True),

            dbc.Container([
                html.H5("District Level"),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(district_corona_controls, md=3),
                        dbc.Col(dcc.Graph(
                            id=f'district_corona_graph',
                            figure=corona_cases_graph("District", ["Delhi"],
                                                      ["Recovered", "Confirmed", "Deceased", "Active"])
                        ), md=9),
                    ],
                    align="center",
                ),

            ], id="district-corona", fluid=True),
        ], fluid=True, style=card_style),
        dbc.Container([
            html.H3("Other Graphs", style={"padding-top": "10px"}),
            html.Hr(),
            dbc.Container([
                html.H5("Weather"),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(city_weather_controls, md=3),
                        dbc.Col(dcc.Graph(
                            id=f'district_weather_graph',
                            figure=weather_graph(["Delhi"], ['maxtempC', 'mintempC', 'avgtempC', 'sunHour', 'uvIndex'])
                        ), md=9),
                    ],
                    align="center",
                ),

            ], id="city-weather", fluid=True),

            dbc.Container([
                html.H5("Air Pollution"),
                html.Hr(),
                dbc.Row(
                    [
                        dbc.Col(city_air_pollution_controls, md=3),
                        dbc.Col(dcc.Graph(
                            id=f'district_pollution_graph',
                            figure=air_pollution_graph(["Delhi"], ['co', 'no2', 'o3', 'pm10', 'pm25', 'so2'])
                        ), md=9),
                    ],
                    align="center",
                ),
            ], id="city-air-pollution", fluid=True),
        ], fluid=True, style=card_style),
        dbc.Container([
            html.H3("Insights", style={"padding-top": "10px"}),
            html.Hr(),
            dbc.Container([
                html.H5("Co-relation of Covid with Population Stats"),
                html.Hr(),
                dbc.Container([
                    html.H6("Number of Deceased per confirmed"),
                    dbc.Container(
                        dash_table.DataTable(
                            id='dcc-table',
                            columns=[{"name": i, "id": i} for i in Dcc.columns],
                            data=Dcc.to_dict('records'),
                            style_cell={'textAlign': 'left', 'whiteSpace': 'normal', 'height': 'auto',
                                        'maxWidth': '400px'},
                            style_cell_conditional=[
                                {
                                    'if': {'column_id': 'Value'},
                                    'textAlign': 'right'
                                }
                            ],
                            style_table={'overflowX': 'auto', 'maxWidth': "600px"}
                        ),
                    )
                ]),
                dbc.Container([
                    html.H6("Number of Deceased per person", style={"padding-top": "10px"}),
                    dbc.Container(
                        dash_table.DataTable(
                            id='Dcp-table',
                            columns=[{"name": i, "id": i} for i in Dcp.columns],
                            data=Dcp.to_dict('records'),
                            style_cell={'textAlign': 'left', 'whiteSpace': 'normal', 'height': 'auto',
                                        'maxWidth': '400px'},
                            style_cell_conditional=[
                                {
                                    'if': {'column_id': 'Value'},
                                    'textAlign': 'right'
                                }
                            ],
                            style_table={'overflowX': 'auto', 'maxWidth': "600px"}
                        ),
                    )
                ]),
                dbc.Container([
                    html.H6("Number of Confirmed per person", style={"padding-top": "10px"}),
                    dbc.Container(
                        dash_table.DataTable(
                            id='Ccp-table',
                            columns=[{"name": i, "id": i} for i in Ccp.columns],
                            data=Ccp.to_dict('records'),
                            style_cell={'textAlign': 'left', 'whiteSpace': 'normal', 'height': 'auto',
                                        'maxWidth': '400px'},
                            style_cell_conditional=[
                                {
                                    'if': {'column_id': 'Value'},
                                    'textAlign': 'right'
                                }
                            ],
                            style_table={'overflowX': 'auto', 'maxWidth': "600px"}
                        ),
                    )
                ])
            ], fluid=True),
            dbc.Container([
                html.H5("Co-relation of Covid with daily variation of Air Pollution and Weather",
                        style={"padding-top": "10px"}),
                html.Hr(),
                dbc.Container(
                    [
                        html.P("In our time series analysis of per capita covid cases and probability of death. We did "
                               "not find any correlation of these parameter with variation of Air Pollutants [CO,NO2,"
                               "SO2,O3,Pm10,Pm25] or Weather [Avg temp, Sun Hours, UV Index].",
                               style={"padding-bottom": "10px"})
                    ])
            ], fluid=True)
        ], id="Findings", fluid=True, style=card_style),
        dbc.Container([
            html.P("This site is designed and developed by Chirag Jasuja and Harshit Gupta.",
                   style={"text-align": "right"})
        ], id="Footer", fluid=True)
    ]

    # Country corona graph related function
    @app.callback(
        dash.dependencies.Output('country_corona_graph', 'figure'),
        [dash.dependencies.Input('day_total_country', 'value')])
    def update_output(graph_type):
        return corona_cases_graph("Country", ["India"],
                                  ["Recovered", "Confirmed", "Deceased", "Active"], graph_type)

    # State corona graph related function
    @app.callback(
        dash.dependencies.Output('state_corona_graph', 'figure'),
        [dash.dependencies.Input('states_dropdown_state_corona', 'value'),
         dash.dependencies.Input('day_total_state', 'value')])
    def update_output(state_list, graph_type):
        if len(state_list) == 0:
            state_list = ["Maharashtra"]
        return corona_cases_graph("State", state_list,
                                  ["Recovered", "Confirmed", "Deceased", "Active"], graph_type)

    # District corona graph related functions
    @app.callback(
        dash.dependencies.Output('district_dropdown_corona', 'options'),
        [dash.dependencies.Input('states_dropdown_district_corona', 'value')])
    def set_cities_options(selected_country):
        if selected_country is None:
            return []
        if type(selected_country) == 'str':
            return [{'label': i, 'value': i} for i in list(df[df["State"] == selected_country]["District"].unique())]
        else:
            return [{'label': i, 'value': i} for country in selected_country for i in
                    list(df[df["State"] == country]["District"].unique())]

    @app.callback(
        dash.dependencies.Output('district_corona_graph', 'figure'),
        [dash.dependencies.Input('district_dropdown_corona', 'value'),
         dash.dependencies.Input('day_total_district', 'value')])
    def update_output(district_list, graph_type):
        if len(district_list) == 0:
            district_list = ["Delhi"]
        return corona_cases_graph("District", district_list,
                                  ["Recovered", "Confirmed", "Deceased", "Active"], graph_type)

    # District Air Pollution
    @app.callback(
        dash.dependencies.Output('district_pollution_graph', 'figure'),
        [dash.dependencies.Input('states_dropdown_district_pollution', 'value')])
    def update_output(city_list):
        if len(city_list) == 0:
            city_list = ["Maharashtra"]
        return air_pollution_graph(city_list, ['co', 'no2', 'o3', 'pm10', 'pm25', 'so2'])

    # District Weather
    @app.callback(
        dash.dependencies.Output('district_weather_graph', 'figure'),
        [dash.dependencies.Input('states_dropdown_district_weather', 'value')])
    def update_output(city_list):
        if len(city_list) == 0:
            city_list = ["Maharashtra"]
        return weather_graph(city_list, ['maxtempC', 'mintempC', 'avgtempC', 'sunHour', 'uvIndex'])

    app.layout = html.Div(children=html_list, style={"background-color": "#F0F0F0", "padding": "10px 15px 30px"})
    app.run_server(debug=True, host="0.0.0.0", port=8051)
