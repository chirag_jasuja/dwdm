import plotly.graph_objects as go
import pandas as pd
from pymongo import MongoClient
import os
from ..DataProcessing.Covariance import get_data


def corona_cases_graph(level, location_list, trace_list, graph_type="Daywise"):
    s = ""
    for loc in location_list[:-1]:
        s += loc + " "
    if len(location_list) > 1:
        s += "and "
    s += location_list[-1]
    fig = go.Figure()

    for location in location_list:
        try:
            if level == "Country":
                df = pd.DataFrame(get_data("Cases", {"Date": {'$regex': '.*'}}))
            else:
                df = pd.DataFrame(get_data("Cases", {f"{level}": location}))
            df["Date"] = pd.to_datetime(df["Date"])
            df.set_index("Date", inplace=True)
            df = df.resample("D").sum()
            if graph_type == "Daywise":
                df = df.diff()
            df["Date"] = df.index.to_frame()
            df["Date"] = pd.to_datetime(df["Date"], dayfirst=False)
            df.index.name = None
            df = df.sort_values(by=['Date'])
            for case_type in trace_list:
                if case_type == "Active":
                    df["Active"] = df["Confirmed"] - df["Recovered"] - df["Deceased"]
                else:
                    df = df[df[case_type] >= 0]
                fig.add_trace(go.Scatter(x=df["Date"], y=df[case_type], name=f"{case_type} in {location}", hoverinfo="x+y",
                                         hovertemplate='%{y}'+f' {case_type}<extra>{location}</extra>'))
        except Exception as e:
            print(df.keys())
            print(e)
    return fig


def weather_graph(location_list, trace_list):
    s = ""
    for loc in location_list[:-1]:
        s += loc + " "
    if len(location_list) > 1:
        s += "and "
    s += location_list[-1]
    fig = go.Figure()

    for location in location_list:
        try:
            df = pd.DataFrame(get_data("Weather", {f"city": location}))
            df["Date"] = pd.to_datetime(df["date"])
            df.set_index("Date", inplace=True)
            df = df.resample("D").mean()
            df["Date"] = df.index.to_frame()
            df["Date"] = pd.to_datetime(df["Date"], dayfirst=False)
            df.index.name = None
            df = df.sort_values(by=['Date'])
            for parameter in trace_list:
                fig.add_trace(go.Scatter(x=df["Date"], y=df[parameter], name=f"{parameter} in {location}", hoverinfo="x+y",
                                         hovertemplate='%{y:.2f}'+f' {parameter}<extra>{location}</extra>'))
        except Exception as e:
            print(df.keys())
            print(e)
    return fig


def air_pollution_graph(location_list, trace_list):
    s = ""
    for loc in location_list[:-1]:
        s += loc + " "
    if len(location_list) > 1:
        s += "and "
    s += location_list[-1]
    fig = go.Figure()
    for location in location_list:
        try:
            df = pd.DataFrame(get_data("AirPollution", {f"city": location}))
            df["Date"] = pd.to_datetime(df["date"])
            df.set_index("Date", inplace=True)
            df = df.resample("D").mean()
            df["Date"] = df.index.to_frame()
            df["Date"] = pd.to_datetime(df["Date"], dayfirst=False)
            df.index.name = None
            df = df.sort_values(by=['Date'])
            for parameter in trace_list:

                fig.add_trace(go.Scatter(x=df["Date"], y=df[parameter], hoverinfo="x+y",name=f"{parameter} in {location}",
                                         hovertemplate='%{y:.2f}'+f' {parameter}<extra>{location}</extra>'))
        except Exception as e:
            print(df.keys())
            print(e)
    return fig
