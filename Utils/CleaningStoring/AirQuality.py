import pandas as pd
from pymongo import MongoClient
import os

client = MongoClient('192.168.29.203', 27017)

def clean(city_name):
    path = f"Data Storage/AirPollution/{city_name}.csv"
    try:
        df = pd.read_csv(path)
    except:
        os.remove(path)
        raise FileNotFoundError
    df = df[df['value'] >= 0]
    df['serial'] = range(1, len(df) + 1)
    df = pd.pivot(df, index=["serial", 'date', "unit"], columns=["parameter"],
                  values=["value"]).reset_index()

    indx = df.keys().to_flat_index()
    df.columns = [x[0] for x in indx[:3]] + [x[1] for x in indx[3:]]
    # [indx[0][0], indx[1][0], indx[2][0], indx[3][0], indx[4][0], indx[5][0], indx[6][1], indx[7][1],
    #               indx[8][1], indx[9][1], indx[10][1], indx[11][1]]
    df[["date", "time"]] = df["date"].str.split("T", expand=True)
    df.drop(columns=["time", "serial"], inplace=True)
    df1 = df.groupby(["date"]).mean()
    city = [city_name for _ in range(df1.shape[0])]
    df1["city"] = city
    df1["date"] = df1.index.to_frame()
    df1["_id"] = df1["date"] + df1["city"]
    df1.rename_axis(None, inplace=True)
    return df1


def store(city_list):
    for city_name in city_list:
        try:
            db = client["DWDM"]
            collection = db["AirPollution"]
            df = clean(city_name)
            for obj in df.to_dict("records"):
                collection.replace_one({"_id": obj["_id"]}, obj, upsert=True)
            # collection.insert_many(df.to_dict('records'))
            path = f"Data Storage/AirPollution/{city_name}.csv"
            os.remove(path)
            print(f"{city_name} stored in database")
        except FileNotFoundError:
            print(f"CleaningStoring/AirQuality: {city_name} not found")
