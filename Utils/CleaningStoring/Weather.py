import pandas as pd
from pymongo import MongoClient
import os

client = MongoClient('192.168.29.203', 27017)


def clean(city_name):
    path = f"Data Storage/Weather/{city_name}.csv"
    try:
        df = pd.read_csv(path)
    except:
        os.remove(path)
        raise FileNotFoundError
    df.drop(columns=["maxtempF", "mintempF", "avgtempF", "totalSnow_cm"], inplace=True)
    df.rename({"date": "_id"}, inplace=True)
    city = [city_name for _ in range(df.shape[0])]
    df["city"] = city
    return df


def store(city_list):
    for city_name in city_list:
        try:
            db = client["DWDM"]
            collection = db["Weather"]
            df = clean(city_name)
            collection.insert_many(df.to_dict('records'))
            path = f"Data Storage/Weather/{city_name}.csv"
            os.remove(path)
            print(f"{city_name} stored in database")
        except FileNotFoundError:
            print(f"CleaningStoring/Weather: {city_name} not found")
