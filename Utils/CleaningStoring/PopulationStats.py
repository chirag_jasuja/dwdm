import pandas as pd
from pymongo import MongoClient
import os

client = MongoClient('192.168.29.203', 27017)


def clean(file_name):
    path = f"../../Data Storage/PopulationStats/{file_name}.csv"
    df = pd.read_csv(path)
    # df.drop(
    #     columns=["Notes", "State Patient Number", "State code", "Source_1",
    #              "Source_2", "Source_3", "Contracted from which Patient (Suspected)", "Patient Number"],
    #     inplace=True)
    df.rename(columns={"District code": "_id"}, inplace=True)
    # print(list(df.keys()))
    return df


def store(file_list):
    for file_name in file_list:
        try:
            db = client["DWDM"]
            collection = db["PopulationStats"]
            df = clean(file_name)
            collection.insert_many(df.to_dict('records'))
            path = f"../../Data Storage/PopulationStats/{file_name}.csv"
            os.remove(path)
            print(f"{file_name} stored in database")
        except FileNotFoundError:
            print(f"CleaningStoring/Cases: {file_name} not found")

clean("india-districts-census-2011")
store(["india-districts-census-2011"])
