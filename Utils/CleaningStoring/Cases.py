import datetime

import pandas as pd
from pymongo import MongoClient
import os

from pytz import timezone

from ..DataScrapping.LastUpdationTime import refresh_last_updation, get_last_updation

client = MongoClient('192.168.29.203', 27017)
# client["DWDM"].drop_collection("Cases")

def clean():
    date = get_last_updation("Cases", "lastUpdated") - datetime.timedelta(days=2)
    path = f"Data Storage/Cases/district.csv"
    date = pd.Timestamp(date)
    df = pd.read_csv(path)
    df["Date"] = pd.to_datetime(df["Date"])
    df.Date = df.Date.dt.tz_localize('UTC').dt.tz_convert('Asia/Kolkata')
    df = df[df["Date"] > date]
    df["Date"] = df["Date"].dt.strftime('%Y-%m-%d')
    df['_id'] = df["Date"] + " " + df["State"] + " " + df["District"]
    return df


def store():
    try:
        db = client["DWDM"]
        collection = db["Cases"]
        df = clean()
        # # add all (only when db empty)
        # collection.insert_many(df.to_dict("records"))
        # # to update
        for obj in df.to_dict("records"):
            collection.replace_one({"_id": obj["_id"]}, obj, upsert=True)
        path = f"Data Storage/Cases/district.csv"
        os.remove(path)
        refresh_last_updation("Cases", "lastUpdated", datetime.datetime.now(tz=timezone('Asia/Kolkata')))
        print(f"Covid Cases stored in database")
    except FileNotFoundError:
        print(f"CleaningStoring/Cases: district.csv not found")


