import os
from datetime import datetime, timedelta
import requests
from pytz import timezone
import json
import pandas as pd
import time
from .LastUpdationTime import refresh_last_updation, get_last_updation


def generate_query(city_name, end_date, start_date='2019-01-01', page=1):
    api_url = f'https://api.openaq.org/v1/measurements?'
    parameter_list = ""
    for p in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 19, 21, 27, 28, 33, 35, 126, 130, 135, 19840]:
        parameter_list = parameter_list + f"parameter={p}&"
    q = f'city={city_name}&{parameter_list}limit=10000&page={page}&offset=0&date_from={start_date}&date_to={end_date}&format=json'
    return api_url + q


def _get_data(city):
    today = datetime.now(tz=timezone('Asia/Kolkata'))
    start_date = get_last_updation("AirPollution", city)
    dict = {}

    print(f"\r{city}, {start_date}",end="")
    while (today - start_date).days > 0:
        total_try = 0
        print(f"\r{city}, {start_date}", end="")
        for page in range(1, 1000):
            query = generate_query(city, end_date=(start_date + timedelta(days=1)).strftime("%Y-%m-%d"),
                                   start_date=start_date.strftime("%Y-%m-%d"), page=page)
            rq = requests.get(query)
            if rq.status_code > 400:
                total_try += 1
                if total_try > 5:
                    print(f"Weather Api not working for {city}")
                    print(query)
                    return dict, start_date

                time.sleep(10)
                continue
            total_try = 0
            js = json.loads(rq.content)
            if len(js["results"]) == 0:
                break
            for data in js['results']:
                for key in data.keys():
                    if key in ["date", "coordinates"]:
                        if key == "date":
                            try:
                                dict["date"].append(data["date"]["utc"])
                            except KeyError:
                                dict[key] = [data["date"]["utc"]]
                        continue
                    try:
                        dict[key].append(data[key])
                    except KeyError:
                        dict[key] = [data[key]]
        start_date += timedelta(days=1)
        # wait to avoid overburdening the api
        # time.sleep(5)
    return dict, start_date


def _save_data(data, city, start_date):
    df = pd.DataFrame.from_dict(data)
    # path = "Data Storage/AirPollution"
    path = "Data Storage/AirPollution"
    os.makedirs(path, exist_ok=True)
    try:
        df1 = pd.read_csv(f"{path}/{city}.csv")
        df = pd.concat([df1, df])
    except:
        pass
    df.to_csv(f"{path}/{city}.csv", index=False)
    refresh_last_updation("AirPollution", city, start_date)


def scrap_data(city_list=None):
    if city_list is None:
        print("Please add cities to scrap")
        return
    print("AirQuality Scraping")
    for city in city_list:
        data, start_date = _get_data(city)
        _save_data(data, city, start_date)
        print(f"\r\t{city} done")


