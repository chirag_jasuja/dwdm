from datetime import datetime, timedelta

import dateutil.parser
from pytz import timezone
import pandas as pd
import json


# each datatype will have a csv file
# each having a key and time
# a key can be city name or general


def refresh_last_updation(datatype, key, date_time):
    try:
        with open(f"Data Storage/LastUpdated/{datatype}.json", "r") as csv_file:
            data = json.load(csv_file)
    except FileNotFoundError:
        data = {}
    data[key] = str(date_time)

    with open(f'Data Storage/LastUpdated/{datatype}.json', 'w') as fp:
        json.dump(data, fp)


def get_last_updation(datatype, key):
    try:
        with open(f"Data Storage/LastUpdated/{datatype}.json", "r") as csv_file:
            data = json.load(csv_file)
        LastUpdated = dateutil.parser.parse(data[key])
    except FileNotFoundError:
        LastUpdated = datetime.now(tz=timezone('Asia/Kolkata')).replace(day=1, month=1, year=2020)
    except KeyError:
        LastUpdated = datetime.now(tz=timezone('Asia/Kolkata')).replace(day=1, month=1, year=2020)
    return LastUpdated


