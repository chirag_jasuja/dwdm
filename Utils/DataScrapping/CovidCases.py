import requests


def scrap_data():
    url = "https://api.covid19india.org/csv/latest/districts.csv"

    r = requests.get(url)
    if r.status_code > 400:
        print("Cases Data Request failed")
    with open(f'Data Storage/Cases/district.csv', 'wb') as f:
        f.write(r.content)
    print("Covid Cases downloaded")
