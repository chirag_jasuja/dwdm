import os
import time
from datetime import datetime, timedelta
import requests
from pytz import timezone
import json
import pandas as pd
from .LastUpdationTime import refresh_last_updation, get_last_updation


def generate_query(city_name, end_date, start_date='2019-01-01'):
    api_key = "c61f18b1b4ae4b44aea115935211503"
    api_url = f'http://api.worldweatheronline.com/premium/v1/past-weather.ashx?key={api_key}&'
    # q = f'{latitude},{longitude}'
    q = f'{city_name}'
    query = f'q={q}&format=json&date={start_date}&enddate={end_date}&tp=24'
    return api_url + query


def get_data(city):
    today = datetime.now(tz=timezone('Asia/Kolkata'))
    start_date = get_last_updation("Weather", city)
    dict = {}
    total_try = 0
    while (today - start_date).days >= 0:
        query = generate_query(city, end_date=today.strftime("%Y-%m-%d"),
                               start_date=start_date.strftime("%Y-%m-%d"))
        rq = requests.get(query)
        if rq.status_code > 400:
            total_try += 1
            if total_try > 5:
                print(f"Weather Api not working for {city}")
                print(query)
                return dict, start_date
            time.sleep(10)
            continue
        total_try = 0
        js = json.loads(rq.content)

        for data in js['data']["weather"]:
            for key in data.keys():
                if key in ["hourly", "astronomy"]:
                    continue
                try:
                    dict[key].append(data[key])
                except KeyError:
                    dict[key] = [data[key]]
        start_date += timedelta(days=35)
    return dict, min(start_date-timedelta(days=35), today)


def save_data(data, city, start_date):
    df = pd.DataFrame.from_dict(data)
    path = "Data Storage/Weather"
    os.makedirs(path, exist_ok=True)
    try:
        df1 = pd.read_csv(f"{path}/{city}.csv")
        df = pd.concat([df1, df])
    except:
        pass
    df.to_csv(f"{path}/{city}.csv", index=False)
    refresh_last_updation("Weather", city, start_date)


def scrap_data(city_list=None):
    if city_list is None:
        print("Please add cities to scrap")
        return
    print("Weather scraping")
    for city in city_list:
        data, start_date = get_data(city)
        save_data(data, city, start_date)
        print(f"\t{city} done")
