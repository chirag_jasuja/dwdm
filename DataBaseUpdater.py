import time
from Utils import DataScrapping, CleaningStoring
import schedule

city_list = ["Delhi", "Ahmedabad", "Mumbai", "Chennai", "Kolkata", "Chandigarh", "Jaipur"]


def update_warehouse():
    DataScrapping.WeatherData.scrap_data(city_list)
    DataScrapping.AirQuality.scrap_data(city_list)
    DataScrapping.CovidCases.scrap_data()

    CleaningStoring.Cases.store()
    CleaningStoring.Weather.store(city_list)
    CleaningStoring.AirQuality.store(city_list)


schedule.every().day.at("10:00").do(update_warehouse)
while True:
    schedule.run_pending()
    time.sleep(300)
